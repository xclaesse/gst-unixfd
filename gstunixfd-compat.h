#pragma once

#include <gst/gst.h>
#include <gst/allocators/allocators.h>

// Backport GStreamer API used by unixfd but missing in 1.16.

void gst_shm_allocator_init_once();
GstAllocator *gst_shm_allocator_get();

GVariant *gst_meta_serialize (const GstMeta * meta);
GstMeta *gst_meta_deserialize(GstBuffer *buffer, GVariant *variant);

#ifndef GST_ELEMENT_REGISTER_DEFINE
#define GST_ELEMENT_REGISTER_DEFINE(element, element_name, rank, type) \
gboolean G_PASTE (gst_element_register_, element) (GstPlugin * plugin) \
{ \
  return gst_element_register (plugin, element_name, rank, type); \
}
#endif

#ifndef GST_ELEMENT_REGISTER
#define GST_ELEMENT_REGISTER(element, plugin) G_PASTE(gst_element_register_, element) (plugin)
#endif

#ifndef GST_ELEMENT_REGISTER_DECLARE
#define GST_ELEMENT_REGISTER_DECLARE(element) \
gboolean G_PASTE(gst_element_register_, element) (GstPlugin * plugin);
#endif
