/* GStreamer unix file-descriptor source/sink
 *
 * Copyright (C) 2023 Netflix Inc.
 *  Author: Xavier Claessens <xavier.claessens@collabora.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * plugin-unixfd:
 *
 * Since: 1.24
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gstunixfd.h"

#include <gio/gunixfdmessage.h>

typedef struct
{
  CommandType type;
  guint32 payload_size;
} Command;

/* For backward compatibility, do not change the size of Command. It should have
 * same size on 32bits and 64bits arches. */
G_STATIC_ASSERT (sizeof (Command) == 8);
G_STATIC_ASSERT (sizeof (MemoryPayload) == 16);
G_STATIC_ASSERT (sizeof (NewBufferPayload) == 56);
G_STATIC_ASSERT (sizeof (ReleaseBufferPayload) == 8);

gboolean
gst_unix_fd_send_command (GSocket * socket, CommandType type, GUnixFDList * fds,
    const gchar * payload, gsize payload_size, GError ** error)
{
  Command command = { type, payload_size };
  GOutputVector vect[] = {
    {&command, sizeof (Command)},
    {payload, payload_size},
  };
  GSocketControlMessage *msg = NULL;
  gint num_msg = 0;
  gboolean ret = TRUE;

  if (fds != NULL) {
    msg = g_unix_fd_message_new_with_fd_list (fds);
    num_msg++;
  }

  if (g_socket_send_message (socket, NULL, vect, G_N_ELEMENTS (vect),
          &msg, num_msg, G_SOCKET_MSG_NONE, NULL, error) < 0) {
    ret = FALSE;
  }

  g_clear_object (&msg);
  return ret;
}

gboolean
gst_unix_fd_receive_command (GSocket * socket, GCancellable * cancellable,
    CommandType * type, GUnixFDList ** fds, gchar ** payload,
    gsize * payload_size, GError ** error)
{
  Command command;
  GInputVector vect = { &command, sizeof (Command) };
  GSocketControlMessage **msg = NULL;
  gint num_msg = 0;
  gint flags = G_SOCKET_MSG_NONE;
  gboolean ret = TRUE;

  if (g_socket_receive_message (socket, NULL, &vect, 1, &msg, &num_msg, &flags,
          cancellable, error) <= 0) {
    return FALSE;
  }

  *type = command.type;
  *payload = NULL;
  *payload_size = 0;

  if (command.payload_size > 0) {
    *payload = g_malloc (command.payload_size);
    *payload_size = command.payload_size;
    if (g_socket_receive (socket, *payload, command.payload_size, cancellable,
            error) < (gssize) command.payload_size) {
      g_free (*payload);
      ret = FALSE;
      goto out;
    }
  }

  if (fds != NULL) {
    *fds = NULL;
    for (int i = 0; i < num_msg; i++) {
      if (G_IS_UNIX_FD_MESSAGE (msg[i])) {
        *fds = g_object_ref (g_unix_fd_message_get_fd_list ((GUnixFDMessage *)
                msg[i]));
        break;
      }
    }
  }

out:
  for (int i = 0; i < num_msg; i++)
    g_object_unref (msg[i]);
  g_free (msg);

  return ret;
}

gboolean
gst_unix_fd_parse_new_buffer (gchar * payload, gsize payload_size,
    NewBufferPayload ** new_buffer, GVariant ** metas)
{
  if (payload == NULL || payload_size < sizeof (NewBufferPayload))
    return FALSE;

  *new_buffer = (NewBufferPayload *) payload;
  gsize struct_size =
      sizeof (NewBufferPayload) +
      sizeof (MemoryPayload) * (*new_buffer)->n_memory;
  if (payload_size < struct_size)
    return FALSE;

  payload += struct_size;
  payload_size -= struct_size;

  *metas = NULL;
  if ((*new_buffer)->metas_payload_size > 0) {
    if (payload_size < (*new_buffer)->metas_payload_size)
      return FALSE;
    *metas =
        g_variant_new_from_data (G_VARIANT_TYPE ("av"), payload,
        (*new_buffer)->metas_payload_size, FALSE, NULL, NULL);
    if (!g_variant_is_normal_form (*metas)) {
      g_clear_pointer (metas, g_variant_unref);
      return FALSE;
    }
  }

  return TRUE;
}

gboolean
gst_unix_fd_parse_release_buffer (gchar * payload, gsize payload_size,
    ReleaseBufferPayload ** release_buffer)
{
  if (payload == NULL || payload_size < sizeof (ReleaseBufferPayload))
    return FALSE;

  *release_buffer = (ReleaseBufferPayload *) payload;

  return TRUE;
}

gboolean
gst_unix_fd_parse_caps (gchar * payload, gsize payload_size, gchar ** caps_str)
{
  if (payload == NULL || payload_size < 1 || payload[payload_size - 1] != '\0')
    return FALSE;

  *caps_str = payload;

  return TRUE;
}

static gboolean
plugin_init (GstPlugin * plugin)
{
  gboolean ret = FALSE;

  ret |= GST_ELEMENT_REGISTER (unixfdsrc, plugin);
  ret |= GST_ELEMENT_REGISTER (unixfdsink, plugin);

  return ret;
}

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    unixfd,
    "Unix file descriptor sink and source",
    plugin_init, VERSION, GST_LICENSE, GST_PACKAGE_NAME, GST_PACKAGE_ORIGIN)
