#include "gstunixfd-compat.h"

#include <gst/video/video.h>

void gst_shm_allocator_init_once()
{
}

GstAllocator *gst_shm_allocator_get()
{
  return gst_fd_allocator_new();
}

static GVariant *
video_meta_serialize (const GstMeta * meta, guchar * version)
{
  GstVideoMeta *vmeta = (GstVideoMeta *) meta;

#if GLIB_SIZEOF_SIZE_T != 8
  /* Convert to guint64 to not serialize using arch dependent gsize */
  guint64 offset64[GST_VIDEO_MAX_PLANES];
  for (int i = 0; i < vmeta->n_planes; i++)
    offset64[i] = vmeta->offset[i];
#else
  const guint64 *offset64 = (guint64 *) vmeta->offset;
#endif

  return g_variant_new ("(iiuuu@at@ai)",
      vmeta->flags,
      vmeta->format,
      vmeta->width,
      vmeta->height,
      vmeta->n_planes,
      g_variant_new_fixed_array (G_VARIANT_TYPE_UINT64, offset64,
          vmeta->n_planes, sizeof (guint64)),
      g_variant_new_fixed_array (G_VARIANT_TYPE_INT32, vmeta->stride,
          vmeta->n_planes, sizeof (gint)));
}

static GstMeta *
video_meta_deserialize (const GstMetaInfo * info, GstBuffer * buffer,
    GVariant * variant, guchar version)
{
  GstVideoMeta *vmeta = NULL;
  GstVideoFrameFlags flags;
  GstVideoFormat format;
  guint width;
  guint height;
  guint n_planes;
  GVariant *offset_va;
  GVariant *stride_va;

  if (version != 0)
    return NULL;

  const gchar *format_string = "(iiuuu@at@ai)";
  if (!g_variant_check_format_string (variant, format_string, FALSE))
    return NULL;

  g_variant_get (variant, format_string,
      &flags,
      &format,
      &width,
      &height,
      &n_planes,
      &offset_va,
      &stride_va);

  if (n_planes > 4)
    goto out;

  gsize n_items;
  const guint64 *offset64 =
      g_variant_get_fixed_array (offset_va, &n_items, sizeof (guint64));
  if (n_items != n_planes)
    goto out;
  const gint *stride =
      g_variant_get_fixed_array (stride_va, &n_items, sizeof (gint));
  if (n_items != n_planes)
    goto out;

#if GLIB_SIZEOF_SIZE_T != 8
  gsize offset[GST_VIDEO_MAX_PLANES];
  for (int i = 0; i < n_planes; i++)
    offset[i] = offset64[i];
#else
  const gsize *offset = (const gsize *) offset64;
#endif

  vmeta =
      gst_buffer_add_video_meta_full (buffer, flags, format, width, height,
      n_planes, (gsize*)offset, (gint*)stride);

out:
  g_variant_unref (offset_va);
  g_variant_unref (stride_va);
  return (GstMeta *) vmeta;
}

static gboolean
is_video_meta(const GstMetaInfo *info)
{
  return g_str_equal(g_type_name(info->type), "GstVideoMeta");
}

GVariant *
gst_meta_serialize (const GstMeta * meta)
{
  g_return_val_if_fail (meta != NULL, NULL);

  if (is_video_meta(meta->info)) {
    guchar version = 0;
    GVariant *va = video_meta_serialize (meta, &version);
    if (va != NULL) {
      return g_variant_new ("(sy*)", g_type_name (meta->info->type), version,
          va);
    }
  }

  return NULL;
}

GstMeta *
gst_meta_deserialize (GstBuffer * buffer, GVariant * variant)
{
  const gchar *name;
  guchar version;
  GVariant *va;
  GstMeta *meta = NULL;

  g_return_val_if_fail (GST_IS_BUFFER (buffer), NULL);
  g_return_val_if_fail (variant != NULL, NULL);

  /* Make sure video meta is registered */
  gst_video_meta_get_info();

  const gchar *format_string = "(&sy*)";
  if (!g_variant_check_format_string (variant, format_string, FALSE)) {
    gchar *s = g_variant_print (variant, TRUE);
    GST_WARNING ("Variant does not hold a meta serialization: %s", s);
    g_free (s);
    return NULL;
  }

  g_variant_get (variant, format_string, &name, &version, &va);

  const GstMetaInfo *info = gst_meta_get_info (name);
  if (info == NULL) {
    GST_WARNING ("Variant %s does not correspond to a registered meta", name);
    goto out;
  }

  if (!is_video_meta(info)) {
    GST_WARNING ("Meta %s does not support deserialization", name);
    goto out;
  }

  meta = video_meta_deserialize (info, buffer, va, version);
  if (meta == NULL) {
    gchar *s = g_variant_print (va, TRUE);
    GST_WARNING ("Failed to deserialize %s meta version %d: %s", name, version,
        s);
    g_free (s);
    goto out;
  }

out:
  g_variant_unref (va);
  return meta;
}
