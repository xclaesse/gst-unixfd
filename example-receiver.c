#include <gst/gst.h>
#include <gst/video/video.h>

int main(int argc, char *argv[]) {
    gst_init(&argc, &argv);
    gst_video_meta_get_info();

    if (argc != 2) {
        g_error("Usage: %s <socket path>", argv[0]);
    }

    GError *error = NULL;
    g_autofree gchar *pipeline_str = g_strdup_printf("unixfdsrc socket-path=%s ! videoconvert ! glimagesink", argv[1]);
    g_autoptr(GstElement) pipeline = gst_parse_launch(pipeline_str, &error);
    g_assert_no_error(error);

    GstStateChangeReturn ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);
    g_assert(ret != GST_STATE_CHANGE_FAILURE);

    g_autoptr(GstBus) bus = gst_element_get_bus(pipeline);
    while(TRUE) {
        g_autoptr(GstMessage) message = gst_bus_timed_pop(bus, GST_CLOCK_TIME_NONE);
    }
}
