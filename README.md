# Build

```sh
meson setup builddir
meson compile -C builddir
```

# Usage

- Sender:
```sh
meson devenv -C builddir gst-launch-1.0 v4l2src device=/dev/video2 ! unixfdsink socket-path=/tmp/unixfd
```

- Receiver:
```sh
meson devenv -C builddir ./gst-unixfd-receiver /tmp/unixfd
```
